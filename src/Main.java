/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fredrik Foss
 */
public class Main {
	
	private static final int STEPS = 1000;
	
	public static void main(String[] args) {
		Simulator sim = new Simulator();
		sim.simulate(STEPS);
	}
}
